# Kernel Ridge Regression for Materials

Tutorial on kernel ridge regression for materials science, created for the block course "Big data and artificial intelligence in materials science" as part of the [Max Planck Graduate Center for Quantum Materials](https://www.quantummaterials.mpg.de).

Topics covered:
- Kernel ridge regression
	- Fundamentals
	- Hyper-Parameter Optimisation
	- Toy implementation
- Representations
- Application to the [NOMAD2018 challenge dataset](https://www.kaggle.com/c/nomad2018-predict-transparent-conductors)
	- Dataset exploration
	- HP Optimisation
	- Discussion of results

## Technicalities

In addition to the prerequisites in `setup.py`, this tutorial requires [`qmmlpack`](https://gitlab.com/qmml/qmmlpack/-/tree/development) on the DEVELOPMENT branch, as well as [`cmlkit`](https://github.com/sirmarcel/cmlkit) and [`cscribe`](https://github.com/sirmarcel/cscribe).

It also requires the environment variable `CML_PLUGINS=cscribe` to be set.

Installing `qmmlpack` by hand, then `pip install cmlkit cscribe` should suffice.

## Todos/Future plans

- Once `cmlkit` supports caching, expand the result analysis section
- Add a bandgap appendix (if feasible)
- Potentially add another representation (ideally a global one)
