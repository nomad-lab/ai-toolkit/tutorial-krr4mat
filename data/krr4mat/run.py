import cmlkit

space = {
    "model": {
        "per": "cell",
        "regression": {
            "krr": {
                "kernel": {
                    "kernel_atomic": {
                        "norm": False,
                        "kernelf": {
                            "gaussian": {
                                "ls": [
                                    "hp_loggrid",
                                    "ls_start",
                                    -10,
                                    2,
                                    25,
                                ]
                            }
                        },
                    }
                },
                "nl": ["hp_loggrid", "nl_start", -20, -10, 21],
            }
        },
        "representation": {
            "ds_soap": {
                "elems": [8, 13, 31, 49],
                "n_max": 6,
                "l_max": 6,
                "cutoff": 5,
                "sigma": ["hp_loggrid", "sigma", -4, 1, 11],
                "rbf": "gto",
            }
        },
    }
}
search = cmlkit.tune.Hyperopt(space, method="tpe")
evaluator = cmlkit.tune.TuneEvaluatorHoldout(
    train="nmd18_hpo_train", test="nmd18_hpo_test", target="fe", lossf="rmse"
)
search = cmlkit.tune.Hyperopt(space, method="tpe")
run = cmlkit.tune.Run(
    search=search,
    evaluator=evaluator,
    stop={"stop_max": {"count": 200}},
    context={"max_workers": 40},
    name="nmd18_hpo",
    caught_exceptions=["QMMLException"]
)
run.prepare()
run.run()
