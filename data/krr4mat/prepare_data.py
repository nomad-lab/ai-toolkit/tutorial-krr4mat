# Used to prepare the example datasets for hyper-parameter optimisation

import numpy as np

np.random.seed(42)
import cmlkit

data = cmlkit.load_dataset("nmd18u")  # obtained from qmml.org

rest, train, test = cmlkit.utility.threeway_split(2400, 800, 200)

train = cmlkit.dataset.Subset.from_dataset(
    data, idx=train, name="nmd18_hpo_train"
)
print(train.n)
train.save()
test = cmlkit.dataset.Subset.from_dataset(data, idx=test, name="nmd18_hpo_test")
print(test.n)
test.save()


train = cmlkit.dataset.Subset.from_dataset(
    data, idx=np.arange(2400), name="nmd18_train"
)
print(train.n)
train.save()
test = cmlkit.dataset.Subset.from_dataset(
    data, idx=np.arange(start=2400, stop=3000), name="nmd18_test"
)
print(test.n)
test.save()
